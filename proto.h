/** @file proto.h
 *  @brief Funktion Prototypen, Header Dateien und globale Variablen.
 *
 *	Dies beinhaltet alle Prototypen der einzelnen Funktionen im gesamten Programm ausgenommen
 *  davon sind die generierten festen Funktionen in der Main C-Datei. Auch beinhaltet sind die
 *	Header zum laufen des Programmes, sowie die ID's der einzelnen Masken und dem Inhalt der
 *	Masken wie Buttons, Labels, Textboxen und Groupboxen. Globale Variablen, Pfade von Dateien
 *	sowie Hinweistexte sind hier beinhaltet.
 *
 *  @author Yousof Kashef
 *  @project Lager
 *  @version v.3.1
 *  @date 24.01.2021
 */

//  @brief Header Dateien
#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <string.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <shellapi.h>
#include "lagerres.h"

/*  @brief Initialisierung der ID f�r die Masken.
 *	Die ID's beginne bei 7000 und k�nnen bei Erweiterung leicht erg�nzt werden.
 */
#define SEARCH_ID			7000
#define MASK_ID_SEARCH		7001
#define TRAGZAHL_ID			7002
#define MASK_ID_TRAGZAHL	7003
#define LIFE_ID				7004
#define MASK_ID_LIFE		7005
#define UMDREHUNG_ID		7006
#define MASK_ID_UMDREHUNG	7007
#define CLEAR_ID			7008
#define OPEN_ID				7009
#define ICON_ID				7010

/*  @brief Initialisierung der Textboxen in den einzelnen Masken.
 *	Die Textboxen beginnen bei 8000 und k�nnen bei Erweiterung leicht erg�nzt werden.
 */
#define SEARCHBAR			8000
#define RESULT				8001
#define FA_TEXT				8002
#define C0_TEXT				8003
#define FR_TEXT				8004
#define X_TEXT				8005
#define Y_TEXT				8006
#define N_TEXT				8007
#define C_TEXT				8008

/*  @brief Initialisierung der Buttons in den Masken.
 *	Die Buttons beginnen bei 9000 und k�nnen bei Erweiterung leicht erg�nzt werden.
 */
#define B_RESULT			9000
#define B_EXIT				9001
#define B_TRANS				9002

/*  @brief Initialisierung der Labels in den Masken @Suche und @Warnung.
 *	Die Labels beginnen bei 10000 und k�nnen bei Erweiterung leicht erg�nzt werden.
 */
#define L_SEARCHBAR			10000
#define L_RESULT			10001
#define L_DEF				10002
#define L_EINHEIT			10003
#define L_WARNING			10004

/*  @brief Initialisierung der Labels der Maske @Lebensdauer.
 *	Die Labels beginnen bei 10500 und k�nnen bei Erweiterung leicht erg�nzt werden.
 */
#define L_FR				10501
#define L_FA				10502
#define L_X					10503
#define L_Y					10504
#define L_C					10505
#define L_N					10506

/*  @brief Initialisierung der Groupboxen in den Masken.
 *	Die Groupboxen beginnen bei 7550 und k�nnen bei Erweiterung leicht erg�nzt werden.
 */
#define BOX_1				7550
#define	BOX_2				7551

/*  @brief Initialisierung der Radiobutton in den Masken.
 *	Die Radiobuttons beginnen bei 9500 und k�nnen bei Erweiterung leicht erg�nzt werden.
 */
#define RB_1				9501
#define RB_2				9502
#define RB_3				9503
#define RB_4				9504
#define RB_5				9505
#define RB_6				9506
#define RB_7				9507
#define RB_8				9508

//  @brief Pfade der Tabllen der Datenbank und der Bedienungsanleitung.
#define Kugellager_Tabelle      "lib/Tabels/Kugellager.csv"
#define Rollenlager_Tabelle     "lib/Tabels/Rollenlager.csv"
#define Tragzahl_Tabelle		"lib/Tabels/Tragzahl.csv"
#define readMe_Text				"lib/readMe.txt"

//  @brief Hinweis Text f�r die Eingabe in den Masken @Tragzahl und @Lebensdauer.
#define HINWEIS "Hinweis:\nAlle Kommata als Punkt angeben. Bei Eingabe mit einem Komma wird die Zahl auf eine Ganzzahl aufgerundet."

//  @param radio Integer Speicherpl�tze f�r Status der Radiobuttons
int radio[8];

//	@param criteria_search Kriterien der Suche
char criteria_search[6][20] ={"Kurzzeichen","Innendurchmesser d","Au�endurchmesser D","Breite B","Tragzahl C","Tragzahl C_0"};
/*  @brief �bergabewerte von @Tragzahl zu @Lebensdauer
 *
 *	@param trans_input �bergabewerte
 *  @param status �bergabestatus
 */
char trans_input[4][10];
int status;

/*  @brief Methode zur �ffnung der Tabellendateien.
 *
 *	@param file_name Dateipfad zur Tabellen.
 *  @param hwnd Windowhandler f�r Exceptionwindow bei fehlgeschlagener Datei�ffnung
 *  @return new_file neu ge�ffnete Datei
 */
FILE* file_open(char file_name[20], HWND hwnd);

/*  @brief Methode zur Ausgabe der Resultate und der Berechnungen ins Protokoll.
 *
 *	@param hwnd Windowhandler f�r Exceptionwindow bei fehlgeschlagener Datei�ffnung
 *  @param result Pointer zum Resultat
 *  @param function Pointer zum Text der Funktion f�r Protokoll
 *  @param extra_line Pointer zum Text der Erg�nzung f�r Protkoll
 *  @return void
 */
void output_protokoll(HWND hwnd, char* result, char* function, char* extra_line);

/*  @brief Methode zum leeren des Protokolls.
 *
 *  @param hwnd Windowhandler f�r Exceptionwindow bei fehlgeschlagener Datei�ffnung
 */
void clear_protokoll(HWND hwnd);

/*  @brief Methode zum �ffnen des Protokolls im Editor
 *
 *	@param hwnd Windowhandler f�r Exceptionwindow bei fehlgeschlagener Datei�ffnung
 */
void open_protokoll(HWND hwnd);

/*  @brief Methode zur Berechnung der Tragzahlfaktoren durch Interpolation der Werte der Tragzahltabelle
 *
 *	@param file Datei zum auslesem der Tabelle
 *	@param data_input �bergebene Werte aus der Maske @Tragzahl
 *  @param Found Resultate der Berechnung
 */
void interpolation(FILE* file,char data_input[3][10], char Found[3][10]);

/*  @brief Methode zur Berechnung der Lebensdauer von Lagern
 *
 *	@param result Resultat der Berechnung
 *  @param input �bergeben Werte aus der Maske @Lebensdauer
 *  @param p Faktor f�r Berechnung
 */
void lifetime(double result[3], double input[6], double p);

/*	@brief Methode zur Suche von Lagern nach Kriterien von Datenbank
 *
 *	@param file Datei zum auslesen der Tabelle
 *  @param phrase String zur Suche aus der Maske @Suche
 *  @param column Kriterium der Suche in den Tabellen, deutet auf die Spaltenzahl in der Tabelle
 *  @param Lager_found Resultate der Suche
 *	@return count Anzahl der gefunden Resultate der Suche
 */
int search(FILE* file, char phrase[25], int column, char Lager_found[4][6][10]);

/*	@brief Methode zur Statusabfrage der Radiobuttons in Maske @Suche
 *
 *	@param hwnd Windowhandler f�r Zugriff auf Radiobuttuns Status
 *	@return column Kriterium f�r die Suche in @lifetime, deutet auf die Spalte in der Tabelle
 */
int get_criteria_search(HWND hwnd);

/*  @brief Methode zur Statusabfrage der Radiobuttons in Maske @Lebensdauer
 *
 *	@param hwnd Windowhandler f�r Zugriff auf Radiobuttuns Status
 *	@return column Kriterium der Berechnung
 */
int get_criteria_life(HWND hwnd);

//	@brief Methode zur Ausgabe der Bedienungsanleitung im Hauptfenster
BOOL text_schreiben(HWND hwnd);

//	@brief Methode der Suchmaske
BOOL APIENTRY search_program(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);

//	@brief Methode der Tragzahlmaske
BOOL APIENTRY tragzahlprogramm(HWND hwnd, UINT msg,WPARAM wParam,LPARAM lParam);

//	@brief Methode der Lebensdauermaske
BOOL APIENTRY lifeprogramm(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);

//	@brief Methode HandleDefaultMessages aus weditres.lib
BOOL WINAPI HandleDefaultMessages(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);
