/** @file masken.c
 *  @brief Funktionen zur Benutzung der Masken
 *
 *	Dies beinhaltet alle Funktionen zur Textausgabe im Hauptfenster, Auffangen des Verhaltens der Buttons
 * 	in den einzelnen Masken, der Status der Radiobuttons, auslesen und einf�gen von Strings in den Textboxen
 *	sowie der Formatierung der Ausdr�cke f�r das Protkoll.
 *
 *  @author Yousof Kashef
 *  @project Lager
 *  @version v.3.1
 *  @date 25.01.2021
 */

#include "proto.h"

//	@brief Methode zur Ausgabe der Bedienungsanleitung im Hauptfenster
BOOL text_schreiben(HWND hwnd){

/*	Die einzelnen Funktion der WIN32 API f�r die Behandlung werden nicht kommentiert.
 *	Ledeglich bei einzelnen Befehlen die, die Funktion der Programms wichtig sind werden kommentiert.
 */
	char text [4000];
	char temp [200];
	char buffer [200];
	int x,y;

	ShowWindow(hwnd,SW_SHOWMAXIMIZED);					// Das Hauptfenster wird auf Max gestellt. Somit wird verhindert, dass Text der Bedienungsanleitung am Rand nicht lesbar ist, wegen verschiedenen Monitorgroessen.
	HDC hdc;
	PAINTSTRUCT ps;
	RECT Rect;

	GetClientRect(hwnd,&Rect);
	InvalidateRect (hwnd,NULL,TRUE);
	hdc = BeginPaint(hwnd,&ps);
	FILE* file = file_open(readMe_Text,hwnd);

	x = 10;
	y = 10;

	sprintf(text,"\0");

	while(fgets(buffer,sizeof(buffer),file)){			// Es wird Zeile f�r Zeile so weiter ausgelesen bis das Ende der Datei @param file erreicht ist. Die ganze Zeile wird in @param buffer gespeichert.

		char* token=strtok(buffer,";");					// Die einzelnen Werte sind durch Semikolons geteilt. Durch die Funktion @func strtok wird in @param token der String bis zum n�chsten Semikolon abgespeichert.

			sprintf(text,token);						// Abspeichern der Daten f�r Ausgabe in @param data.
			token=strtok(NULL,";");
			TextOut(hdc,x,y,text,strlen(text));			// Ausgabe des Textes im Hauptfenster
			y = y+20;									// Zeilensprung
	}

	fclose(file);
	EndPaint(hwnd,&ps);
	UpdateWindow(hwnd);

return 0;
}

//	@brief Methode der Suchmaske
BOOL APIENTRY search_program(HWND hwnd, UINT msg,WPARAM wParam,LPARAM lParam){

	int count = 0;
	int column = 0;
	int empty = 0;
	char phrase[25];
	char Lager_found [4][6][10];
	char extra_line[100];
	char output_window[500];
	char temp_window[50];
	char protokoll_output[500];
	char protokoll_temp[50];
	char function[50];

	//	Formatierung des Textes f�r die Ausgabe
	sprintf(output_window,"\0");
	sprintf(protokoll_output,"\0");
	sprintf(extra_line,"Lager	|	d	|	D	|	B	|	C	|	C_0");

	switch (msg){

	case WM_INITDIALOG:

	// Vorselektierung der Buttons 1 und 7 als default
		CheckDlgButton(hwnd,RB_1,BST_CHECKED);
		CheckDlgButton(hwnd,RB_7,BST_CHECKED);

	case WM_COMMAND:

		switch (LOWORD(wParam)){


		case B_RESULT:

			column = get_criteria_search(hwnd);																// Aufrufen der @func get_criteria_search zur Bestimmung der Spaltenzahl f�r die Tabelle

			radio[6] = IsDlgButtonChecked(hwnd,RB_7);														// Abfrage der Status von den Radiobuttons 7 und 8 f�r Lagerartbestimmung
			radio[7] = IsDlgButtonChecked(hwnd,RB_8);

			Edit_GetText(GetDlgItem(hwnd,SEARCHBAR),phrase,25);												// Auslesen der Textbox f�r die gesuchte @param phrase

			if(strcmp(phrase,"") == 0){																		// Abfrage ob die @param phrase leer ist
				MessageBox(hwnd,"Keine Eingabe, Felder sind leer!","Fehler",MB_OK | MB_ICONEXCLAMATION);	// Falls die @param phrase leer ist, wird eine Warnungsnachricht ausgegeben.
				empty = 1;																					// Status der Abfrage @param empty wird zu 1 gestzt
				break;
			}

			if(empty == 1) break;																			// Abbrechen der Suche

			if(radio[6] == 1){																				// Abfrage ob der Radiobutton 7 gew�hlt wurde f�r Lager Bestimmung

				FILE* file = file_open(Kugellager_Tabelle,hwnd);											// �ffnen der Datei @param file mit @func file_open des Lagertypes
				count = search(file,phrase,column,Lager_found);												// Bestimmung der Anzahl der gefundenen L�sungen @param count
				sprintf(function,"Lagersuche (Kugellager) -> %s",criteria_search[column-1]);				// Funktion abspeicherung f�r Protkoll
			}
			if(radio[7] == 1){																				// Abfrage ob der Radiobutton 8 gew�hlt wurde f�r Lager Bestimmung

				FILE* file = file_open(Rollenlager_Tabelle,hwnd);											// �ffnen der Datei @param file mit @func file_open des Lagertypes
				count = search(file,phrase,column,Lager_found);												// Bestimmung der Anzahl der gefundenen L�sungen @param count
				sprintf(function,"Lagersuche (Zylinderrollenlager) -> %s",criteria_search[column-1]);		// Funktion abspeicherung f�r Protkoll

			}
			if(count == 0){
				Edit_SetText(GetDlgItem(hwnd,RESULT),"nichts gefunden");									// Falls es keine L�sungen gefunden wurde wird das Suchergebnis als nichts gefunden angezeigt
			}

			else{
				/* Formatierung des Textes f�r die Ausgabe im Protokoll und im Fenster der Suche
				 * Protokoll und Anzeigefenster haben verschiedene Formatierungen und somite verschieden abgespeichert.
				 */
				for(int i=0;i<count;i++){
					sprintf(temp_window,"%s | %s | %s | %s | %s | %s\r\n",Lager_found[i][0],Lager_found[i][1],Lager_found[i][2],Lager_found[i][3],Lager_found[i][4],Lager_found[i][5]);
					strcat(output_window,temp_window);
					sprintf(protokoll_temp,"%s	|	%s	|	%s	|	%s	|	%s	|	%s\r\n",Lager_found[i][0],Lager_found[i][1],Lager_found[i][2],Lager_found[i][3],Lager_found[i][4],Lager_found[i][5]);
					strcat(protokoll_output,protokoll_temp);

				}

				output_protokoll(hwnd,protokoll_output, function, extra_line);								// Ausgabe des Suchergebnisses im Protokol mit @func output_protokoll
				Edit_SetText(GetDlgItem(hwnd,RESULT),output_window);										// Ausgabe des Suchergebnisses im Fenster @Suche
				count = 0;
			}
			break;


		case B_EXIT:
			PostMessage(hwnd,WM_CLOSE,0,0);
		}


	}
	return(HandleDefaultMessages(hwnd,msg,wParam,lParam));
}

//	@brief Methode der Tragzahlmaske
BOOL APIENTRY tragzahlprogramm(HWND hwnd, UINT msg,WPARAM wParam,LPARAM lParam){

	int empty = 0;
	char data_input[3][10];
	char output_window[1000];
	char temp_window[1000];
	char function [500];
	char extra_line [1000];

	//	Formatierung des Textes f�r die Ausgabe
	char Tragzahl_Found [3][10] = {"0"};
	sprintf(function,"Tragzahl");


	double input;

	switch(msg){

	case WM_COMMAND:

		switch(LOWORD(wParam)){

		case B_TRANS:

			status = 0;																									// Status der �bergabe an das Fenster @Lebensdauer
			break;

		case B_RESULT:

			FILE* file =file_open(Tragzahl_Tabelle,hwnd);																// �ffnen der Datei @param file mit @func file_open der Tragzahltabelle

			// Auslesen der Textboxen f�r die Speicherung der Werte in @param data
			Edit_GetText(GetDlgItem(hwnd,FA_TEXT),data_input[0],10);
			Edit_GetText(GetDlgItem(hwnd,FR_TEXT),data_input[1],10);
			Edit_GetText(GetDlgItem(hwnd,C0_TEXT),data_input[2],10);

			for(int i = 0; i<=2; i++){

				if(strcmp(data_input[i],"") == 0){																		// Abfrage ob die Textboxen leer sind

					MessageBox(hwnd,"Keine Eingabe, Felder sind leer!","Fehler",MB_OK | MB_ICONEXCLAMATION);			// Falls eineTextbox leer ist, wird eine Warnungsnachricht ausgegeben.
					empty = 1;																							// Status der Abfrage @param empty wird zu 1 gesetzt
					break;
				}
			}

			if (empty == 1) break;																						// Abbrechen des Auftrags
			interpolation(file,data_input,Tragzahl_Found);																// Berechnung der Tragzahlen mit @func interpolation

			/* Formatierung des Textes f�r die Ausgabe im Protokoll und im Fenster der Suche
			 * Protokoll und Anzeigefenster haben verschiedene Formatierungen und somite verschieden abgespeichert.
			 */
			sprintf(output_window,"e = %s | X = %s | Y = %s",Tragzahl_Found[0],Tragzahl_Found[1],Tragzahl_Found[2]);
			sprintf(temp_window,"%s	|	%s	|	%s	\n",Tragzahl_Found[0],Tragzahl_Found[1],Tragzahl_Found[2]);

			sprintf(extra_line,"F_a = %s\nF_r = %s\nC_0 = %s\n",data_input[0],data_input[1],data_input[2]);
			strcat(extra_line,"e	|	X	|	Y");

			Edit_SetText(GetDlgItem(hwnd,RESULT),output_window);														// Ausgabe des Berechnungsergebnisses im Fenster @Tragzahl

			output_protokoll(hwnd, temp_window, function, extra_line);													// Ausgabe des Berechnungsergebnisses im Protokol mit @func output_protokoll

			// Abspeichern der eingegebenen Werte sowie das Berechnungsergbnisses in @param trans_input f�r die Maske @Lebensdauer
			sprintf(trans_input[0],data_input[0]);
			sprintf(trans_input[1],data_input[1]);
			sprintf(trans_input[2],Tragzahl_Found[1]);
			sprintf(trans_input[3],Tragzahl_Found[2]);

			break;

		case B_EXIT:
			PostMessage(hwnd,WM_CLOSE,0,0);
		}
	}
	return(HandleDefaultMessages(hwnd,msg,wParam,lParam));
}

//	@brief Methode der Lebensdauermaske
BOOL APIENTRY lifeprogramm(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam){

	char data_input[6][10];

	int Lager_art;
 	int empty = 0;
	double output_window[3];
	double input[6];
	double p;

	char function [50];
	char extra_line [100];
	char temp_window[100];
	char text[1000];

	//	Formatierung des Textes f�r die Ausgabe
	sprintf(extra_line,"L_10h	|	L_10	|	P");
	sprintf(function," Ledensdauer");
	sprintf(text,"\0");

	switch(msg){

	case WM_INITDIALOG:

		if (status == 0){																						// Abfrage von �bergabestatus von Maske @Tragzahl

		// �bernahme der �bergabewerte und einsetzen in der Maske @Lebensdauer
		Edit_SetText(GetDlgItem(hwnd,FA_TEXT),trans_input[0]);
		Edit_SetText(GetDlgItem(hwnd,FR_TEXT),trans_input[1]);
		Edit_SetText(GetDlgItem(hwnd,X_TEXT),trans_input[2]);
		Edit_SetText(GetDlgItem(hwnd,Y_TEXT),trans_input[3]);

		status = 1;																								// Zur�cksetzen von �bergabestatus auf 0
		}

		CheckDlgButton(hwnd,RB_7,BST_CHECKED);


	case WM_COMMAND:

		switch(LOWORD(wParam)){

		case B_RESULT:

			radio[6], radio[7]=0;

			Lager_art = get_criteria_life(hwnd);																// Aufrufen der @func get_criteria_life zur Bestimmung der Lagerart

			// Auslesen der Textboxen f�r die Speicherung der Werte in @param data_input
			Edit_GetText(GetDlgItem(hwnd,FA_TEXT),data_input[0],10);
			Edit_GetText(GetDlgItem(hwnd,FR_TEXT),data_input[1],10);
			Edit_GetText(GetDlgItem(hwnd,C_TEXT),data_input[2],10);
			Edit_GetText(GetDlgItem(hwnd,X_TEXT),data_input[3],10);
			Edit_GetText(GetDlgItem(hwnd,Y_TEXT),data_input[4],10);
			Edit_GetText(GetDlgItem(hwnd,N_TEXT),data_input[5],10);

			for(int i = 0; i<=5; i++){

				if(strcmp(data_input[i],"") == 0){																// Abfrage ob die Textboxen leer sind

					MessageBox(hwnd,"Keine Eingabe, Felder sind leer!","Fehler",MB_OK | MB_ICONEXCLAMATION);	// Falls eineTextbox leer ist, wird eine Warnungsnachricht ausgegeben.
					empty = 1;																					// Status der Abfrage @param empty wird zu 1 gesetzt
					break;
				}
			}

			if(empty == 1) break;																				// Abbrechen des Auftrags


			for(int i = 0; i <=5 ;i++){

				input[i] = atof(data_input[i]);																	// Formatierung der Eingabewerte @data_input vom Typ cahr in @param input vom Typ double f�r weitere Berechnung
			}

			// Bestimmen des Faktors @param p f�r Berechnung der Lebensdauer nach Art des Lagers
			if(Lager_art == 1){

				p=3;
			}
			if(Lager_art == 2){

				p=10/3;
			}

			lifetime(output_window, input,p);																	// Berechnung der Lebensdauer mit @func lifetime

			/* Formatierung des Textes f�r die Ausgabe im Protokoll und im Fenster der Suche
			 * Protokoll und Anzeigefenster haben verschiedene Formatierungen und somite verschieden abgespeichert.
			 */
			sprintf(text,"Die Lebenszeit des Lagers mit 90%% Wahrscheinlichkeit\r\nund unter normalen Einsatzbedingungen:\r\n1. Betriebs Lebensdauer L_10h:		%.1lf Stunden\r\n2. Nominelle Lebensdauer L_10:		%.1lf Stunden\r\n3. Dynamische �quivalente Lagerlast P:	%.1lf kN",output_window[2],output_window[1],output_window[0]);
			sprintf(temp_window,"%.1lfh	|	%.1lfh	|	%.1lfkN\n\n",output_window[2],output_window[1],output_window[0]);

			output_protokoll(hwnd,temp_window,function,extra_line);												// Ausgabe des Berechnungsergebnisses im Protokol mit @func output_protokoll
			Edit_SetText(GetDlgItem(hwnd,RESULT),text);															// Ausgabe des Berechnungsergebnisses im Fenster @Tragzahl

			break;

		case B_EXIT:
			PostMessage(hwnd,WM_CLOSE,0,0);
		}
	}

	return(HandleDefaultMessages(hwnd,msg,wParam,lParam));
}


