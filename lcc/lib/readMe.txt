Bedienungsanleitung

Dieses Programm dient als Hilfestellung zur Berechnung und der Suche von Lagern und orientiert sich an den Modulen Maschinenelemente und Konstruktionslehre zur Anwendung. Die zwei Lagerarten, die im Programm verwendet werden, sind das Kugellager und das Zylinderrollenlager.
Im Folgenden werden einige Warnungen und Regeln aufgelistet, die zur Verwendung des Programmes wichtig sind. Anschliessend werden die einzelnen Funktionen und Reiter beschrieben.

Hinweise:
Alle reellen Zahlen in den Reitern "Tragzahl" und "Lebensdauer" werden mit dem Trennzeichen Punkt und nicht Komma angegeben (z.B. 3.4). Im Reiter "Suche" soll das Trennzeichen Komma benutzt werden (z.B. 43,6).

Die Eingabe-Taste darf mit nur mit Kombination mit der Tabulator-Taste verwendet werden und nur wenn der Focus auf den Buttons ist. Falls die Eingabe-Taste ansonsten gedrueckt wird, wird der Reiter geschlossen und die eingegebenen Werte werden verloren gegangen. Die Default Einstellung der Eingabetaste in der WIN32 API ist das Schliessen des Fensters. 

Es wird empfohlen am Anfang jeder Sitzung das Protokoll zu leeren und am Ende der Sitzung an einem gewuenschten Ort zu Speichern oder den Text zu kopieren. Somit wird verhindert, dass die Uebersicht ueber den Inhalt des Protokolls verloren geht.

Das Protokoll wird nach Standard im Ordner Downloads erstellt und gespeichert. Falls dies nicht moeglich ist wird das Protokoll im Lcc-Ordner des Programms erstellt.

Die Tabellen der Lager, die als Datenbank dienen fuer die Suchfunktion duerfen nur ueber einen Editor geoeffnet werden und nicht mit EXCEL, da EXCEL die Werte mit der Autofill Funktion als Daten ansieht und die Werte aendert und somit wird die Datenbank fehlerhaft. Falls dies passiert ist ein Backup fuer die Datenbank unter dem Pfad : ("lcc/lib/backup") gespeichert wobei diese Schreibgeschuetzt sind und somit nur zur Kopie dienen.

Anwendung:

* Reiter "Suche": Im Reiter "Suche" hat man die Moeglichkeit Lager nach ihrer Art und Kriterien zu suchen; falls mehrere Treffer zu einem Kriterium auftauchen, werden alle Treffer aufgelistet. Der Suchauftrag wird mit dem Fertig begonnen. Mit dem Button Schliessen wird das Fenster "Suche" geschlossen. 

* Reiter "Tragzahl": Im Reiter "Tragzahl" werden die Faktoren zur Berechnung der Lagerlast berechnet durch Eingeben der axialen und radialen Lagerkraefte des Lagers sowie der statischen Tragzahl des eingesetzten Lagers. Wie in den Hinweisen schon erwaehnt sollen die eingegebenen Werte mit einem Punkt und nicht mit einem Komma angegeben werden. Mit dem Button Fertig wird Berechnung begonnen. Mit dem Button uebernehmen werden die eingebebenen und berechneten Werte zur weiteren Berechnung der Lebensdauer des Lagers verwendet. Dies dient zur Erleichterung der Benutzung des Programmes und ist nicht notwendig zur Benutzung der Funktion Lebensdauer. Mit dem Button Schliessen wird das Fenster geschlossen.

* Reiter "Lebensdauer": Im Reiter "Lebensdauer" wir die Lebensdauer und weitere Werte fuer die eingegebenen Kraefte und Faktoren sowie Umdrehungen berechnet. Mit dem Button Fertig wird die Berechnung begonnen. Mit dem Button Schliessen wird das Fenster geschlossen.

* Reiter "Protokoll": Im Reiter "Protokoll" sind die Unterreiter "leeren" und "oeffnen" vorhanden. Mit leeren ist es moeglich das Protokoll von aelteren Daten zu reinigen und somit ein sauberes Protokoll fuer die Sitzung zu starten. Dabei ist zu beachten, dass die aelteren Auftraege endgueltig geloescht werden aus dem Protokoll und somit empfehlenswert eine Kopie nach jeder Sitzung zu erstellen. Mit dem oeffnen ist es moeglich das Protokoll mit einem Editor zu oeffnen.
 
 
 
 
