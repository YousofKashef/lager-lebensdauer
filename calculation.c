/** @file calculation.c
 *  @brief Funktionen zur Berechnung und Dateiverwaltung
 *
 *	Dies beinhaltet alle Funktionen zum �ffnen und bearbeiten von Dateien wie die Tabellen
 *	der Datenbank der Suchfunktion und die Verwaltung des Protokolls. Desweiteren sind die
 *  Funtkionen zur Suche und Berechnung der Lebensdauer und Tragzahlen beinhaltet sowie die
 *  Funtktionen zur Kriterienbestimmung bei der Suche und den restlichen Berechnungen.
 *	Es wurde bei diesen auf Globalit�t der Parameter und Attribute vollkommen verzichtet um
 *  es Programmiergerecht zu gestallten.
 *
 *  @author Yousof Kashef
 *  @project Lager
 *  @version v.3.1
 *  @date 24.01.2021
 */

#include "proto.h"

/*  @brief Methode zur �ffnung der Tabellendateien.
 *
 *	@param file_name Dateipfad zur Tabellen.
 *  @param hwnd Windowhandler f�r Exceptionwindow bei fehlgeschlagener Datei�ffnung
 *  @return new_file neu ge�ffnete Datei
 */
FILE* file_open(char file_name[20], HWND hwnd){

	FILE* new_file= fopen(file_name,"r");		// �ffnen der Datei zum Auslesen.

	if(new_file == NULL){						// Abfrage ob die Datei ge�ffnet wurde und bei exception eine Fehlmeldung anzeigen und das Programm schliessen
		MessageBox(hwnd,"Datei konnte nicht ge�ffnet werden!","Fehler",MB_OK | MB_ICONEXCLAMATION);
		exit(1);
	}
	return new_file;
}

/*  @brief Methode zur Ausgabe der Resultate und der Berechnungen ins Protokoll.
 *
 *	@param hwnd Windowhandler f�r Exceptionwindow bei fehlgeschlagener Datei�ffnung
 *  @param result Pointer zum Resultat
 *  @param function Pointer zum Text der Funktion f�r Protokoll
 *  @param extra_line Pointer zum Text der Erg�nzung f�r Protkoll
 * @return void
 */
void output_protokoll(HWND hwnd, char *result, char* function, char *extra_line){

	// n�tige Variablen zur Bestimmung des jetzigen Datums und Uhrzeit f�r Protkoll

	time_t rawtime;
	char current_time [256];
	struct tm* timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);

	sprintf(current_time, "Logtime ->[%d.%d.%d - %d:%d:%d]\n\n",timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);		// Abspeichern des Datums und Uhrzeit


	char pfad [256];
	char user [256];
	sprintf(user,"%s",getenv("USERNAME"));							// Namen des jetzigen Benutzers des Systems zu ermitteln und in user speichern
	sprintf(pfad,"C:/Users/%s/Downloads/Protokoll.txt",user);		// Generierung des Datei Pfades mit dem jetzigen Benutzers zum den Downloadordner f�r das Protokoll und in pfad speichern

	FILE* new_file= fopen(pfad,"a");								// �ffnen der Datei new_file mit dem Pfad pfad f�r Erg�nzung

	if(new_file == NULL){											// Abfrage ob die Datei ge�ffnet wurde und bei exception wird versucht das Protokoll im lcc Ordner zu �ffnen. Dies ist eine Absicherung f�r eine fehlerhafte ermittlung des Benutzers
		FILE* new_file = fopen("Protokoll.txt","a");
	}
	// Das Programm wird hier nicht geschlossen da, es nicht Funktional das Programm hindert.

	// Ausdrucken in das Protokoll
	fprintf(new_file,current_time);
	fprintf(new_file,"Funktion: %s\nResultat:\n",function);
	fprintf(new_file,"%s\n",extra_line);
	fprintf(new_file,result);
	fprintf(new_file,"********************************************************************************************\n\n");

	fclose(new_file);												// Schliessen der Datei
}

/*  @brief Methode zum leeren des Protokolls.
 *
 *  @param hwnd Windowhandler f�r Exceptionwindow bei fehlgeschlagener Datei�ffnung
 *  @return new_file geleerte Datei
 */
void clear_protokoll(HWND hwnd){

	char pfad [256];
	char user [256];
	sprintf(user,"%s",getenv("USERNAME"));												// Namen des jetzigen Benutzers des Systems zu ermitteln und in user speichern
	sprintf(pfad,"C:/Users/%s/Downloads/Protokoll.txt",user);							// Generierung des Datei Pfades mit dem jetzigen Benutzers zum den Downloadordner f�r das Protokoll und in pfad speichern

	FILE* new_file= fopen(pfad,"w");													// �ffnen der Datei new_file mit dem Pfad pfad f�r schreiben bzw. in diesem Fall l�schen des Inhalts

	if(new_file == NULL){																// Abfrage ob die Datei ge�ffnet wurde und bei exception wird versucht das Protokoll im lcc Ordner zu �ffnen. Dies ist eine Absicherung f�r eine fehlerhafte ermittlung des Benutzers
		FILE* new_file = fopen("Protokoll.txt","w");
	}

	// Das Programm wird hier nicht geschlossen da, es nicht Funktional das Programm hindert.
	fprintf(new_file,"");																// leeren der Datei
	fclose(new_file);																	// schliessem der Datei

	MessageBox(hwnd,"Protokoll ist leer!","Fehler",MB_OK | MB_ICONEXCLAMATION);			// eine Nachricht erscheint, dass das Protokoll jetzt geleert wurde.
}

/*  @brief Methode zum �ffnen des Protokolls im Editor
 *
 *	@param hwnd Windowhandler f�r Exceptionwindow bei fehlgeschlagener Datei�ffnung
 */
void open_protokoll(HWND hwnd){

	char pfad [256];
	char user [256];

	int exception;

	sprintf(user,"%s",getenv("USERNAME"));																	// Namen des jetzigen Benutzers des Systems zu ermitteln und in user speichern
	sprintf(pfad,"C:/Users/%s/Downloads/Protokoll.txt",user);												// Generierung des Datei Pfades mit dem jetzigen Benutzers zum den Downloadordner f�r das Protokoll und in pfad speichern

	exception = (int)	ShellExecute(NULL, "open", pfad, NULL,NULL,1);										// Abspeicherung des R�ckgabewertes des Befehls ShellExecute.

	if(exception <= 32){																					// Falls der R�ckgabewert exception kleiner als 32 ist, ist ein Fehleraufgetreten bei der Ausf�hrung
		exception = 33;																						// Falls die �ffnung des Protokolls Fehlhaft war, wird versucht das Protokol im lcc Ordner zu �ffnen mit der gleichen Prozedur, dabei wird der Wert von exception auf einen h�heren Wert gesetztf�r weitere Abfragen
		exception = (int)	ShellExecute(NULL, "open", "Protokoll.txt", NULL,NULL,1);

		if(exception <= 32){																				// Falls der R�ckgabewert exception kleiner als 32 ist, ist ein Fehleraufgetreten bei der Ausf�hrung
			MessageBox(hwnd,"Datei konnte nicht ge�ffnet werden!","Fehler",MB_OK | MB_ICONEXCLAMATION);		// eine Nachricht mit Fehlgeschalgenem Versucht wird dargestellt
		}
		// Das Programm wird hier nicht geschlossen da, es nicht Funktional das Programm hindert.
	}
}

/*	@brief Methode zur Statusabfrage der Radiobuttons in Maske @Suche
 *
 *	@param hwnd Windowhandler f�r Zugriff auf Radiobuttuns Status
 *	@return column Kriterium f�r die Suche in @lifetime, deutet auf die Spalte in der Tabelle
 */
int get_criteria_search(HWND hwnd){


	int column = 0;

	// Abspeicher des Status der Radiobuttons. Ein Radiobutton Status ist entweder 0 f�r nicht gew�hlt oder 1 f�r gew�hlt.
	radio[0] = IsDlgButtonChecked(hwnd,RB_1);
	radio[1] = IsDlgButtonChecked(hwnd,RB_2);
	radio[2] = IsDlgButtonChecked(hwnd,RB_3);
	radio[3] = IsDlgButtonChecked(hwnd,RB_4);
	radio[4] = IsDlgButtonChecked(hwnd,RB_5);
	radio[5] = IsDlgButtonChecked(hwnd,RB_6);

	// Abfrage nach dem Status zur Unterscheidung der Suchkriterien. Die Nummer des Radiobuttons wird als Spaltenzahl abgespeichert.
	for(int i = 0; i <= 5; i++){

		if(radio[i] == 1){
			column = i+1;
			break;
		}
	}
	return column;
}

/*  @brief Methode zur Statusabfrage der Radiobuttons in Maske @Lebensdauer
 *
 *	@param hwnd Windowhandler f�r Zugriff auf Radiobuttuns Status
 *	@return column Kriterium der Berechnung
 */
int get_criteria_life(HWND hwnd){

	int column = 0;

	// Abspeicher des Status der Radiobuttons. Ein Radiobutton Status ist entweder 0 f�r nicht gew�hlt oder 1 f�r gew�hlt.
	radio[6] = IsDlgButtonChecked(hwnd,RB_7);
	radio[7] = IsDlgButtonChecked(hwnd,RB_8);

	// Abfrage nach dem Status zur Unterscheidung der Suchkriterien. Die Nummer des Radiobuttons wird als Spaltenzahl abgespeichert.
	if(radio[6] == 1){
		column =1;
	}
	else{
		column =2;
	}

	return column;
}

/*  @brief Methode zur Berechnung der Tragzahlfaktoren durch Interpolation der Werte der Tragzahltabelle
 *
 *	@param file Datei zum auslesem der Tabelle
 *	@param data_input �bergebene Werte aus der Maske @Tragzahl
 *  @param Found Resultate der Berechnung
 */
void interpolation(FILE* file, char data_input[3][10], char Found[3][10]){

	char data [6][4][6];
	char buffer [50];

	int lines = 0;

	// Berechnung der Faktoren Fa/C0 und Fa/Fr f�r Vergleich und weitere Berechnung.
	double Fa_C0 = atof(data_input[0])/atof(data_input[2]);
	double Fa_Fr = atof(data_input[0])/atof(data_input[1]);

	fgets(buffer,sizeof(buffer),file);				// Auslesen der ersten Zeile der Tabelle ohne Speicherung. In der ersten Zeile der Tabelle sind keine Werte gespeichert.


	while(fgets(buffer,sizeof(buffer),file)){		// Es wird Zeile f�r Zeile so weiter ausgelesen bis das Ende der Datei @param file erreicht ist. Die ganze Zeile wird in @param buffer gespeichert.

		char* token=strtok(buffer,";");				// Die einzelnen Werte sind durch Semikolons geteilt. Durch die Funktion @func strtok wird in @param token der String bis zum n�chsten Semikolon abgespeichert.

		for(int i=0;i<= 3;i++){						// Der Rest der einzelnen Werte in der Zeile wird gleichermassen ausgelesen bis zum Ende der Zeile.

			sprintf(data[lines][i],token);			// Abspeichern der Daten f�r Vergleiche und Berechnungen in @param data.

			token=strtok(NULL,";");
		}
		lines++;
	}

	// Interpolierung der berechneten Faktoren mit den ausgelesenen Daten.
	for(int i=0;i<=5;i++){

		if(Fa_C0 >= atof(data[i][0]) && Fa_C0 <= atof(data[i+1][0])){		// Abfrage ob der Faktor @param Fa_C0 zwischen zwei Werten in der ersten Spalte von @param data auftritt.

			if((Fa_C0-atof(data[i][0])) < (atof(data[i+1][0])-Fa_C0)){		// Interpolierung zum n�heren Wert in der Spalte

				if(Fa_Fr > atof(data[i][1])){								// Vergleich ob der Faktor @param Fa_Fr groesser ist als der Faktor der zweiten Spalte des bestimmten Faktors Fa/C0.

					for(int lines = 0;lines<= 2;lines++){					// Bei wahrem Vergleich werden die Werte in der Zeile des bestimmten Faktors Fa/C0 abgespeichert in @param data

						sprintf(Found[lines],data[i][lines+1]);
					}
					break;
				}
				else{														//	Bei unwahrem Vergleich werden feste Werte in @param data gespeichert. Die Werte deuten auf ein Lager das nur axial Beansprucht wird.

					sprintf(Found[0],"keins");
					sprintf(Found[1],"1");
					sprintf(Found[2],"0");
					break;
				}
			}
			else{
				if(Fa_Fr > atof(data[i+1][1])){								// Vergleich ob der Faktor @param Fa_Fr groesser ist als der Faktor der zweiten Spalte des bestimmten Faktors Fa/C0.

					for(int lines = 0;lines<= 2;lines++){					// Bei wahrem Vergleich werden die Werte in der Zeile des bestimmten Faktors Fa/C0 abgespeichert in @param data

						sprintf(Found[lines],data[i+1][lines+1]);
					}
					break;
				}
				else{														//	Bei unwahrem Vergleich werden feste Werte in @param data gespeichert. Die Werte deuten auf ein Lager das nur axial Beansprucht wird.

					sprintf(Found[0],"keins");
					sprintf(Found[1],"1");
					sprintf(Found[2],"0");
					break;
				}
			}
		}

		else{																// Bei Fehlgeschlagener Interpolation kann die L�sung nicht bestimmt werden und es wird in @param data keine Werte gespeichert.
			for(int j =0;j<3;j++){
				sprintf(Found[j],"keins");
			}
		}
	}
	fclose(file);															// schliessen der Datei @param file
}

/*	@brief Methode zur Suche von Lagern nach Kriterien von Datenbank
 *
 *	@param file Datei zum auslesen der Tabelle
 *  @param phrase String zur Suche aus der Maske @Suche
 *  @param column Kriterium der Suche in den Tabellen, deutet auf die Spaltenzahl in der Tabelle
 *  @param Lager_found Resultate der Suche
 *	@return count Anzahl der gefunden Resultate der Suche
 */
int search(FILE* file,char phrase [10], int column, char Lager_found[4][6][10]){

	char data[45][6][10];
	char buffer [50];
	int lines = 0;
	int count = 0;

	fgets(buffer,sizeof(buffer),file);										// Es wird Zeile f�r Zeile so weiter ausgelesen bis das Ende der Datei @param file erreicht ist. Die ganze Zeile wird in @param buffer gespeichert.

	while(fgets(buffer,sizeof(buffer),file)){								// Es wird Zeile f�r Zeile so weiter ausgelesen bis das Ende der Datei @param file erreicht ist. Die ganze Zeile wird in @param buffer gespeichert.

		char* token=strtok(buffer,";");										// Die einzelnen Werte sind durch Semikolons geteilt. Durch die Funktion @func strtok wird in @param token der String bis zum n�chsten Semikolon abgespeichert.

		for(int i=0;i<= 5;i++){												// Der Rest der einzelnen Werte in der Zeile wird gleichermassen ausgelesen bis zum Ende der Zeile.

			sprintf(data[lines][i],token);									// Abspeichern der Daten f�r Vergleiche und Berechnungen in @param data.
			token=strtok(NULL,";");
		}
		lines++;
	}

	for(int lines =0;lines <= 44;lines++){

		if(strcmp(data[lines][column-1],phrase) == 0){						// Vergleich von @param phrase mit dem String in der Spalte @column der einzelnen Zeilen @param lines.

			for(int i = 0;i<=5 ; i++){

				strcpy(Lager_found[count][i],data[lines][i]);				// Abspeichern der restlichen Werte der Zeile @param lines in @param data
			}
			count++;														// Aufz�hlung der gefundenen L�sungen in @param count
		}
	}

	fclose(file);															// schliessen der Datei @param file
	return count;															// r�ckgabe der Anzahl der gefundenen L�sungen
}

/*  @brief Methode zur Berechnung der Lebensdauer von Lagern
 *
 *	@param result Resultat der Berechnung
 *  @param input �bergeben Werte aus der Maske @Lebensdauer
 *  @param p Faktor f�r Berechnung
 */
void lifetime(double result[3], double input[6], double p){

	// Alle berechnete Werte werden auf eine Nachkommastelle gerundet.

	result[0]= roundf((input[3]*input[1]+input[0]*input[4])*10)/10;						// Berechnung der dynamischen Lagerlast nach der Formel P = X*Fr + Y*Fa

	result[1]= roundf(pow((input[2]/result[0]),p)*10)/10;								// Berechnung der nominellen Lebensdauer nach der Formel L_10 = (C/P)^p

	result[2]= roundf(pow((input[2]/result[0]),p)*(pow(10,6)/(input[5]*60))*10)/10;		// Berechnung der Betriebslebensdauer nach der Formel L_10_h = L_10*( 10^6/(60*n));
}




